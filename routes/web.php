<?php

// App\User::create([
// 	'name' => 'Osmin',
// 	'email' => 'martinezmonjaras2015@gmail.com',
// 	'password' => bcrypt('123123'),
// 	'role_id' => 2
// ]);

// DB::listen(function($query) {
// 	echo "<pre>{$query->sql}</pre>";
// });

// Route::get('job', function() {
// 	dispatch(new App\Jobs\SendEmail);

// 	return "Listo";
// });

Route::get('roles', function() {
	return \App\Role::with('users')->get();
});

Route::get('/', ['as' => 'home', 'uses' => 'PagesController@home']);

Route::get('saludos/{nombre?}', ['as' => 'saludos', 'uses' => 'PagesController@saludo'])->where('nombre', '[A-Za-z]+');

Route::resource('mensajes', 'MessagesController');
Route::resource('usuarios', 'UsersController');

Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
