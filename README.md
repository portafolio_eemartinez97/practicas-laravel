# Practicas Laravel

Este proyecto fue desarrollado con la finalidad de poner en práctica los 
conocimientos adquiridos en la formación para poder crear aplicaciones con 
Laravel, el cual fue construido basado en buenas practicas aplicando patrones 
de diseño (con la final de crear proyectos robustos y escalables).

## Temas que destacan dentro del proyecto

* Rutas
* Vistas
* Modelos
* Controladores
* Requests
* Validaciones de formularios
* Middlewares
* Migraciones
* Eloquent 
* Usuarios y roles
* Policies
* Relaciones (uno a uno, uno a muchos, muchos a muchos, polimorficas)
* Homestead
* Seeders
* Paginación
* Caché
* Decoradores e Interfaces
* View Presenters
* Queues & Jobs
* Unit Testing
* Sockets
* DB (Maria DB)
* Entre otros

## Demo

[http://laravel.eemartinez97.com/](http://laravel.eemartinez97.com/)


**Email:** admin@email.com

**Password:** admin