{{csrf_field()}}

<p><label for="avatar">
	<input type="file" name="avatar" class="form-control">
	{!! $errors->first('avatar', '<span class="error">:message</span>') !!}
</label></p>

<p><label for="name">
	Nombre
	<input type="text" name="name" value="{{ $user->name or old('name')}}" class="form-control">
	{!! $errors->first('name', '<span class="error">:message</span>') !!}
</label></p>

<p><label for="email">
	Email
	<input type="text" name="email" value="{{ $user->email or old('email')}}" class="form-control">
	{!! $errors->first('email', '<span class="error">:message</span>') !!}
</label></p>

@unless($user->id)
	
	<p><label for="password">
		Password
		<input type="password" name="password" class="form-control">
		{!! $errors->first('password', '<span class="error">:message</span>') !!}
	</label></p>

	<p><label for="password_confirmation">
		Password Confirm
		<input type="password" name="password_confirmation" class="form-control">
		{!! $errors->first('password_confirmation', '<span class="error">:message</span>') !!}
	</label></p>
	
@endunless

<div class="checkbox">
	@foreach($roles as $id => $name) 
		<label>
			<input 
				type="checkbox" 
				value="{{ $id }}" 
				name="roles[]"
				{{ $user->roles->pluck('id')->contains($id) ? 'checked' : '' }}
				>
			{{$name}}
		</label>
	@endforeach
</div>
{!! $errors->first('roles', '<span class="error">:message</span>') !!}
<hr>