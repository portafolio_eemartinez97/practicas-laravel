@extends('layout')

@section('contenido')
	
	<h1>Crear nuevo usuario</h1>

	<form method="POST" 
		action="{{ route('usuarios.store') }}"
		enctype="multipart/form-data">
		
		@include('users.form', ['user' => new App\User])	

		<input type="submit" value="Guardar" class="btn btn-primary">
	</form>

@stop