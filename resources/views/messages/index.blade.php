@extends('layout')

@section('contenido')
	
	<h1>Todos los mensajes</h1>

	<table class="table">
		<thead>
			<tr>
				<td>ID</td>
				<td>Nombre</td>
				<td>Email</td>
				<td>Mensaje</td>
				<td>Notas</td>
				<td>Etiquetas</td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody>
			@foreach($messages as $message)
			<tr>
				<td>{{ $message->id }}</td>
				<td>{{ $message->present()->userName() }}</td>
				<td>{{ $message->present()->userEmail() }}</td>
				<td>{{ $message->present()->link() }}</td>
				<td>{{ $message->present()->notes() }}</td>
				<td>{{ $message->present()->tags() }}</td>
				<td>
					<a class="btn btn-info btn-xs" href="{{ route('mensajes.edit', $message->id) }}">Editar</a>
					<form style="display: inline;" method="POST" action="{{ route('mensajes.destroy', $message->id) }}">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button class="btn btn-danger btn-xs" type="submit">Eliminar</button>
					</form>
				</td>
			</tr>
			@endforeach

			{{ $messages->fragment('hash')->appends(request()->query())->links('pagination::simple-default') }}
		</tbody>
	</table>
@stop