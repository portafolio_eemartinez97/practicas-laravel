require('./bootstrap');

$('form').on('submit', function() {
	$(this).find('input[type=submit]').attr('disabled', true);
});


Echo.channel('messages-channel')
	.listen('MessageWasReceived', (data) => {
		let message = data.message;
		let html = `
			
			<tr>
				<td>${message.id}</td>
				<td>${message.nombre}</td>
				<td>${message.email}</td>
				<td>${message.mensaje}</td>
				<td></td>
				<td></td>
				<td>
					<a class="btn btn-info btn-xs" href="/mensajes/${message.id}/edit">Editar</a>
					<form style="display: inline;" method="POST" action=/mensajes/${message.id}">
						<input type="hidden" name="_token" value="${Laravel.csrfToken}"></input>
						<input type="hidden" name="_method" value="DELETE"></input>
						<button class="btn btn-danger btn-xs" type="submit">Eliminar</button>
					</form>
				</td>
			</tr>

		`;

		$(html).hide().prependTo('tbody').fadeIn();
	});