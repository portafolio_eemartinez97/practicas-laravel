<?php

namespace App\Http\Controllers;

use Mail;
use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Mail\TuMensajeFueRecibido;
use App\Events\MessageWasReceived;
use App\Repositories\MessagesInterface;
use Illuminate\Events\Dispatcher as Event;
use Illuminate\Contracts\View\Factory as ViewFactory;


class MessagesController extends Controller
{

    protected $messages;
    protected $view;
    protected $redirect;

    function __construct(MessagesInterface $messages, ViewFactory $view, Redirector $redirect ) 
    {
        $this->view = $view;
        $this->messages = $messages;
        $this->redirect = $redirect;
        $this->middleware('auth', ['except' => ['create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = $this->messages->getPaginated();

        return $this->view->make('messages.index', ['messages' => $messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->view->make('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Event $event)
    {
        $message = $this->messages->store($request);

        $event->fire(new MessageWasReceived($message));
        
        // //Redireccionar
        return $this->redirect->route('mensajes.create')
            ->with('info', 'Hemos recibido tu mensaje')
        ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $message = $this->messages->findById($id);
 
        return $this->view->make("messages.show", compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = $this->messages->findById($id);

        return $this->view->make('messages.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $message = $this->messages->update($request, $id);

        //Redireccionar
        return $this->redirect->route('mensajes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->messages->destroy($id);

        return $this->redirect->route('mensajes.index');
    }
}
