<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMessageRequest;

class PagesController extends Controller
{

    public function home() {
    	return view('home');
    }
}
