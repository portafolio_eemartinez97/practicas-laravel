<?php

namespace App\Listeners;

use Mail;
use App\Mail\MensajeRecibido;
use App\Events\MessageWasReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationToTheOwner implements ShouldQueue
{


    /**
     * Handle the event.
     *
     * @param  MessageWasReceived  $event
     * @return void
     */
    public function handle(MessageWasReceived $event)
    {
        $message = $event->getMessage();

        Mail::to('osminerick@gmail.com', 'Erick Martinez')->send(new MensajeRecibido($message));
    }
}



